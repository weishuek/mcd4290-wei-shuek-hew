//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "Question 1" + "\n"//empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    
    let arr = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25];
    
    let PositiveOdd = [];
    let NegativeEven = [];
    
    for (let i = 0; i < arr.length; i++){
        if(arr[i] > 0 && arr[i] % 2 === 1){
            PositiveOdd.push(arr[i]);
        }
    }
        for (let i = 0; i < arr.length; i++){
        if(arr[i] < 0 && arr[i] % 2 === 0){
            NegativeEven.push(arr[i]);
        }
    }
    
    output += "Positive Odd: " + PositiveOdd + "\n" + "Negative Even: " + NegativeEven

    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "Question 2" + "\n"
    
    //Question 2 here 
    
    let f1 = 0
    let f2 = 0
    let f3 = 0
    let f4 = 0
    let f5 = 0
    let f6 = 0
    
    for(let i = 0; i<60000; i++){
        let dieFace = Math.floor((Math.random() * 6) + 1);
        if(dieFace ===1){
            f1++;
        }
        else if(dieFace ===2){
            f2++;
        }
        else if(dieFace ===3){
            f3++;
        }
        else if(dieFace ===4){
            f4++;
        }
        else if(dieFace ===5){
            f5++;
        }
        else{
            f6++;
        }
    }
    output += "Frequency of die rolls: " + "\n" + "1. " + f1 + "\n" + "2. " + f2 + "\n" + "3. " + f3 + "\n" + "4. " + f4 + "\n" + "5. " + f5 + "\n" + "6. " + f6
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "Question 3" + "\n"
    
    //Question 3 here 
    let frequencies = [1,2,3,4,5,6,7]
    for(let i = 0; i<=60000; i++){
        let dieFace = Math.floor((Math.random() * 6) + 1);
            frequencies[dieFace]++
    }
    output += "Frequency of die rolls: " + "\n" + "1. " + frequencies[1] + "\n" + "2. " + frequencies[2] + "\n" + "3. " + frequencies[3] + "\n" + "4. " + frequencies[4] + "\n" + "5. " + frequencies[5] + "\n" + "6. " + frequencies[6]
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "Question 4" + "\n"
    
    //Question 4 here 
    let dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total: 60000,
        Exceptions: ""
    }
    for(let i = 1; i <= dieRolls.Total; i++){
        let dieFace = Math.floor((Math.random() * 6) + 1);
        dieRolls.Frequencies[dieFace];
    }
    for(let prop in dieRolls.Frequencies){
        if(dieRolls.Frequencies > 11000 ||dieRolls.Frequencies < 9900){
            dieRolls.Exceptions 
        }
    }
    output += dieRolls.Frequencies
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "Question 5" + "\n"
    
    //Question 5 here 
    
    let person = {
        name: "Jane",
        income: 127050,
}
    let totalTax = 0;
    
    if( person.income < 18200 ){
        totalTax = 0;
    }
    else if( person.income < 37000){
        totalTax = (person.income - 18200)*0.19;
    }
    else if( person.income < 90000){
        totalTax = 3572 + (person.income - 37000)*0.325;
    }
    else if( person.income < 180000){
        totalTax = 20797 + (person.income - 90000)*0.37;
    }
    else if( person.income > 180001){
        totalTax = 54097 + person.income*0.45;
    }

    output += person.name + "'s" + " income is: " + person.income+ ", and her tax owed is: " + totalTax.toFixed(2) + "."
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output
}